

resource "google_bigquery_table" "default" {

  dataset_id = var.dataset_id
  table_id   = var.table_name
  project    = var.project_d
  schema     = jsonencode(jsondecode(file("../../dataset.json")))

} 