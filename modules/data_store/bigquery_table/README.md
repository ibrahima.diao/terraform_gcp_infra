## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [google](#provider\_google) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [google_bigquery_table.default](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/bigquery_table) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_dataset_id"></a> [dataset\_id](#input\_dataset\_id) | dataset name in biguery | `string` | n/a | yes |
| <a name="input_project_d"></a> [project\_d](#input\_project\_d) | the gcp project id | `string` | n/a | yes |
| <a name="input_table_name"></a> [table\_name](#input\_table\_name) | the name table in the dataset | `string` | n/a | yes |

## Outputs

No outputs.
