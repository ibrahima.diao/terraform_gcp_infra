

variable "table_name" {
  description = "the name table in the dataset"
  type        = string
}

variable "dataset_id" {
  description = "dataset name in biguery"
  type        = string
}

variable "project_d" {

  description = "the gcp project id "
  type        = string

}