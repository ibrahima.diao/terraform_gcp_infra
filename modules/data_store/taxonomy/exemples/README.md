## Requirements

No requirements.

## Providers

No providers.

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_create_taxonomy"></a> [create\_taxonomy](#module\_create\_taxonomy) | ../../../modules/data_store/taxonomy_resource | n/a |

## Resources

No resources.

## Inputs

No inputs.

## Outputs

No outputs.
