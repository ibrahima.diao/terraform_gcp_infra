

resource "google_data_catalog_taxonomy" "my_taxonomy" {
  for_each               = local.indexed_taxonomies
  project                = var.project_id
  region                 = each.value.taxonomy_region
  display_name           = each.value.taxonomy_name
  description            = each.value.taxonomy_description
  activated_policy_types = ["FINE_GRAINED_ACCESS_CONTROL"]

}

resource "google_data_catalog_policy_tag" "basic_policy_tag" {
  for_each     = local.policies_in_taxonomies
  taxonomy     = google_data_catalog_taxonomy.my_taxonomy[each.value.tax.taxonomy_name].id
  display_name = each.value.pol.display_name
  description  = each.value.pol.policy_description
  depends_on   = [google_data_catalog_taxonomy.my_taxonomy]
}