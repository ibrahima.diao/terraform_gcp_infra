

variable "project_id" {

  type = string

}

locals {
  config_file            = try(file("../../../policy.yaml"))
  config                 = yamldecode(local.config_file)
  all_taxonomies         = tolist([for i in local.config.taxonomies : i])
  indexed_taxonomies     = { for taxonomy in local.all_taxonomies : taxonomy.taxonomy_name => taxonomy }
  policies_in_taxonomies = merge([for tax in local.indexed_taxonomies : { for pol in tax.policies : "${tax.taxonomy_name}-${pol.display_name}" => merge({ tax = tax, pol = pol }) }]...)
}




