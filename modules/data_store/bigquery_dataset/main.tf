

resource "google_bigquery_dataset" "datasets" {
  count = length(var.dataset_id_with_description)

  dataset_id  = var.dataset_id_with_description[count.index]["id"]
  description = var.dataset_id_with_description[count.index]["description"]
  location    = var.location
  project     = var.project_id

}
