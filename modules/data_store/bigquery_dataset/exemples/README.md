## Requirements

No requirements.

## Providers

No providers.

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_create_bigquery_dataset"></a> [create\_bigquery\_dataset](#module\_create\_bigquery\_dataset) | ../../../modules/data_store/bigquery_dataset_resource | n/a |

## Resources

No resources.

## Inputs

No inputs.

## Outputs

No outputs.
