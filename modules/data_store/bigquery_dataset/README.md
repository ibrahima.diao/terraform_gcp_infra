## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [google](#provider\_google) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [google_bigquery_dataset.datasets](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/bigquery_dataset) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_dataset_id_with_description"></a> [dataset\_id\_with\_description](#input\_dataset\_id\_with\_description) | A list of all the dataset ID's and their descriptions. | `list` | `[]` | no |
| <a name="input_location"></a> [location](#input\_location) | dataset location | `any` | n/a | yes |
| <a name="input_project_id"></a> [project\_id](#input\_project\_id) | the id project | `any` | n/a | yes |

## Outputs

No outputs.
