resource "google_storage_bucket" "state" {
  name     = "terraform-state-ibra"
  location = "europe-west1"

}

resource "google_storage_bucket_object" "state_env" {
  name       = var.name
  bucket     = google_storage_bucket.state.name
  content    = " "
  depends_on = [google_storage_bucket.state]

}