

resource "google_service_account" "store_user" {
  account_id   = var.account
  project      = var.project_id
  display_name = var.service_account_name
}

resource "google_project_iam_member" "iam" {
  project = var.project_id
  count   = length(var.rolesList)
  role    = var.rolesList[count.index]
  member  = "serviceAccount:${google_service_account.store_user.email}"
}