variable "rolesList" {
  type    = list(string)
  default = ["roles/storage.admin", "roles/pubsub.admin"]
}

variable "project_id" {
  type = string
}

variable "service_account_name" {
  type        = string
  description = "the name of service account"
}

variable "account" {
  description = "the name to use for email adress"
  type        = string
}

