

#add roles 

variable "roles" {
  description = "the roles that will garanted the service account"
  type        = list(string)
  default     = []
}

variable "members" {

  description = "the email for gcp member"
  type        = list(string)
  default     = []
}

locals {
  member_role_combinations = flatten([
    for member in var.members :
    [
      for role in var.roles :
      {
        member = member
        role   = role
      }
    ]
  ])
}

