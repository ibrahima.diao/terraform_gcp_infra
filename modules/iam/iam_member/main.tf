
# add iam member 

resource "google_project_iam_member" "iam" {
  count = length(local.member_role_combinations)

  project = "vote-for-innov"
  member  = local.member_role_combinations[count.index].member
  role    = local.member_role_combinations[count.index].role

  condition {
    title       = "expires_after_2019_12_31"
    description = "Expiring at midnight of 2019-12-31"
    expression  = "request.time < timestamp(\"2023-05-26T12:30:00Z\")"
  }
}