## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [google](#provider\_google) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [google_cloud_run_service.test](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/cloud_run_service) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_image"></a> [image](#input\_image) | image in contaner registry | `string` | n/a | yes |
| <a name="input_location"></a> [location](#input\_location) | the project\_id | `string` | n/a | yes |
| <a name="input_name_app"></a> [name\_app](#input\_name\_app) | the name of app deploy in cloud run | `string` | n/a | yes |

## Outputs

No outputs.
