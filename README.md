
## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_bigquery_data"></a> [bigquery\_data](#module\_bigquery\_data) | ./modules/bigquery_ressource | n/a |
| <a name="module_composer_cluster"></a> [composer\_cluster](#module\_composer\_cluster) | ./modules/composer_ressource | n/a |
| <a name="module_dataproc_cluster"></a> [dataproc\_cluster](#module\_dataproc\_cluster) | ./modules/dataproc_ressource | n/a |

## Resources

No resources.

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_project_id"></a> [project\_id](#input\_project\_id) | n/a | `string` | `"vote-for-innov"` | no |
| <a name="input_region"></a> [region](#input\_region) | n/a | `string` | `"europe-west1"` | no |

## Outputs

test

