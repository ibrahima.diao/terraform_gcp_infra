
provider "google" {
  credentials = file("/tmp/serviceaccount.json")
  project     = var.project_id
  region      = var.region

}


module "create_bigquery_dataset" {

  source     = "../../../../modules/data_store/bigquery_dataset"
  location   = "europe-west1"
  project_id = "vote-for-innov"
  dataset_id_with_description = [

    {
      id          = "dataset3"
      description = "Premier dataset"
    },
    {
      id          = "dataset4"
      description = "Deuxième dataset"
    }
  ]

}

