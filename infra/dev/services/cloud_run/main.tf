
provider "google" {
  credentials = file("/tmp/serviceaccount.json")
  project     = var.project_id
  region      = var.region

}

module "cloud_run_service" {
  source   = "../../../../modules/services/cloud_run"
  location = "europe-west1"
  name_app = "tesrun"
  image    = "gcr.io/vote-for-innov/app"

}
