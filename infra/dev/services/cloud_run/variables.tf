

variable "project_id" {

  default = "vote-for-innov"
}

variable "region" {

  default = "europe-west1"
}