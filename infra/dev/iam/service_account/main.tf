
provider "google" {
  credentials = file("/tmp/serviceaccount.json")
  project     = var.project_id
  region      = var.region

}

module "account" {

  source               = "../../../../modules/iam/service_account"
  rolesList            = ["roles/storage.admin", "roles/pubsub.admin"]
  project_id           = "vote-for-innov"
  service_account_name = "triggerpusub"
  account              = "triggerpusubaccount"

}

