
provider "google" {
  credentials = file("/tmp/serviceaccount.json")
  project     = var.project_id
  region      = var.region

}

module "members_storage" {
  source  = "../../../../modules/iam/iam_member"
  members = ["user:sadi.delhaye@atos.net", "user:ibrahima.diao@atos.net"]
  roles   = ["roles/storage.objectCreator", "roles/storage.objectAdmin"]
}
